var theme = theme || {};
theme.uikit = theme.uikit || {};

theme.uikit.clearErrorsSpec = function() {
    beforeEach( function() {
        this.row = helper.themes.uikit.getRow();
        this.input = this.row.querySelector( 'input' );
        this.controls = this.row.querySelector( '.uk-form-controls' );
        this.theme = new LiveValidator.themes.UIkit( this.input );
    } );

    it( 'already has errors', function() {
        this.input.classList.add( 'uk-form-danger' );
        var p = document.createElement( 'p' );
        p.classList.add( 'uk-form-help-block' );
        p.innerHTML = 'Error';

        var div = document.createElement( 'div' );
        div.classList.add( 'errors' );
        div.classList.add( 'uk-text-danger' );
        div.classList.add( 'uk-margin-small-left' );
        div.appendChild( p );

        this.controls.appendChild( div );
        expect( this.row ).toContainHtml( '<div class="errors uk-text-danger uk-margin-small-left">' +
        '<p class="uk-form-help-block">Error</p></div>' );

        this.theme.clearErrors();
        expect( this.row ).not.toContainText( 'Error' );
        expect( this.row ).not.toContainElement( '.errors' );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
    } );

    it( 'having no errors', function() {
        expect( this.row ).not.toContainElement( '.errors' );
        this.theme.clearErrors();
        expect( this.row ).not.toContainElement( '.errors' );
        expect( this.input ).not.toHaveClass( 'uk-form-danger' );
    } );
};
