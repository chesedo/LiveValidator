// Get namespace ready
var LiveValidator = LiveValidator || {};
LiveValidator.themes = LiveValidator.themes || {};

LiveValidator.themes.UIkit = function UIkitTheme( element, options ) {

    // Scope-safe the object
    if ( !( this instanceof LiveValidator.themes.UIkit ) ) {
        return new LiveValidator.themes.UIkit( element, options );
    }

    this.element = element;
    this.options = LiveValidator.utils.extend(
        {},
        {
            danger: 'uk-form-danger',
            success: 'uk-form-success',
            parentSelector: '.uk-form-row',
            controlsSelector: '.uk-form-controls'
        },
        options
    );
    this.asterisk = null;
    this.controls = null;
    this.parentEl = LiveValidator.utils.parentSelector( this.element, this.options.parentSelector );

    if ( this.parentEl ) {
        this.asterisk = this.parentEl.querySelector( 'span.uk-text-danger' );
        this.controls = this.parentEl.querySelector( this.options.controlsSelector );
    }
};

LiveValidator.themes.UIkit.prototype.markRequired = function() {
    if ( !this.asterisk && this.parentEl ) {
        this.asterisk = document.createElement( 'span' );
        this.asterisk.innerHTML = ' *';
        LiveValidator.utils.addClass( this.asterisk, 'uk-text-danger' );
        LiveValidator.utils.appendChild( this.parentEl.querySelector( 'label' ), this.asterisk );
    }
};
LiveValidator.themes.UIkit.prototype.unmarkRequired = function() {
    if ( this.parentEl ) {
        LiveValidator.utils.removeChild( this.parentEl.querySelector( 'label' ), 'span' );
        this.asterisk = null;
    }
};
LiveValidator.themes.UIkit.prototype.setMissing = function() {
    LiveValidator.utils.removeClass( this.element, this.options.success );
    LiveValidator.utils.addClass( this.element, this.options.danger );
};
LiveValidator.themes.UIkit.prototype.unsetMissing = function() {
    LiveValidator.utils.removeClass( this.element, this.options.danger );
    LiveValidator.utils.addClass( this.element, this.options.success );
};
LiveValidator.themes.UIkit.prototype.clearErrors = function() {
    this.unsetMissing();
    LiveValidator.utils.removeChild( this.controls, '.errors' );
};
LiveValidator.themes.UIkit.prototype.addErrors = function( errors ) {

    // Remove old errors
    this.clearErrors();

    // Create ul
    var div = document.createElement( 'div' );
    LiveValidator.utils.addClass( div, 'errors' );
    LiveValidator.utils.addClass( div, 'uk-text-danger' );
    LiveValidator.utils.addClass( div, 'uk-margin-small-left' );

    // Add each error
    errors.forEach( function( error ) {
        var p = document.createElement( 'p' );
        LiveValidator.utils.addClass( p, 'uk-form-help-block' );
        p.innerHTML = error;
        div.appendChild( p );
    } );

    // Add errors to row
    LiveValidator.utils.appendChild( this.controls, div );
    this.setMissing();
};
