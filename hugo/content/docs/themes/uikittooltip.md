---
date: 2016-11-30T10:52:24+02:00
description: UIkit Tooltip theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: UIkit Tooltip
type: theme
theme: UIkitTooltip
---
This UIkit theme uses UIkit's tooltip component, therfore be sure to include jQuery and UIkit's JS as well as the tooltip's CSS and JS
