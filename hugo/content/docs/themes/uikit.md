---
date: 2016-11-30T10:52:24+02:00
description: UIkit theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: UIkit
type: theme
theme: UIkit
---
This is the basic UIkit theme that does not need jQuery or any other UIkit JS components. It uses the `uk-form-help-block` together with `uk-text-danger` to display the errors to the users
