---
date: 2016-11-30T10:52:24+02:00
description: Bootstrap 3 Tooltip theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: Bootstrap 3 Tooltip
type: theme
theme: Bootstrap3Tooltip
---
This Bootstrap 3 theme uses the tooltip by Bootstrap to display the errors. It therefore needs jQuery and the Bootstrap JS code.
