---
date: 2016-07-20T14:26:22+02:00
description: Use these instructions to get Live Validator for your project
menu:
    main:
        weight: 1
title: Download
type: doc
---

## Zip Download
To get the latest distribution version as a ZIP use the following button. Or if you want to use the source, then that is available too.

{{% button href="https://gitlab.com/chesedo/LiveValidator/builds/artifacts/%s/download?job=Distribute" type="primary" hrefParamSub="version" %}}Download{{% /button %}}
{{% button href="https://gitlab.com/chesedo/LiveValidator/repository/archive.zip?ref=%s" type="success" hrefParamSub="version" %}}Source{{% /button %}}

### HTML Markup Example (Vanilla JS)
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page Title</title>
        <!-- Include the theme style if needed -->
        <link rel="stylesheet" href="/css/LiveValidator/themes/live-validator-theme-default.min" charset="utf-8">
    </head>
    <body>
        <form action="#" method="post">
            <!-- ... -->
        </form>
        <!-- For vanilla JS -->
        <script src="js/LiveValidator/js-live-validator.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/translations/en-us.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/themes/live-validator-theme-default.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            var validationGroup = document.getElementsByTagName( 'form' ).getLiveValidator();
        </script>
    </body>
</html>
```

### HTML Markup Example (jQuery)
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page Title</title>
        <!-- Include the theme style if needed -->
        <link rel="stylesheet" href="/css/LiveValidator/themes/live-validator-theme-default.min" charset="utf-8">
    </head>
    <body>
        <form action="#" method="post">
            <!-- ... -->
        </form>

        <!-- If you want to use jQuery -->
        <script src="js/jquery.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/jquery-live-validator.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/translations/en-us.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/themes/live-validator-theme-default.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            $( function() {
                var validationGroup = $( 'form' ).LiveValidator();
            } );
        </script>
    </body>
</html>
```
{{% alert %}}In both cases a **plug-in object** gets returned that you can use to interact with all the inputs{{% /alert %}}

## Bower
If you are using Bower to manage your assets, then you will find the plugin listed as `live-validator`.

### Install
It can be installed using the following
```
bower install --save live-validator
```
{{% alert %}}This will give you the [source file structure]({{<ref "#source-structure">}}) with `src` only{{% /alert %}}

## File structure
The zip file will contain either the distribution code that is ready to use as is. Or if you downloaded the source, you will have to make sure that you include the correct files for the plug-in to function properly.

### Distribution structure
Folder | Description
-------|----------------------------------------------------------------------
js     | Contains compiled version of the JavaScripts
css    | Contains the processed styles for the themes, if the theme needs extra styles

```markup
/js
    <!-- Contains the core, plug-in (and jQuery plugin if needed), tester and auto checks detection code -->
    /js-live-validator.js
    /js-live-validator.min.js
    /jquery-live-validator.js
    /jquery-live-validator.min.js

    <!-- The JavaScript for all the available themes -->
    /themes
        /live-validator-theme-default.js
        /live-validator-theme-default.min.js
        /live-validator-theme-bootstrap3.js
        /live-validator-theme-bootstrap3.min.js
        /live-validator-theme-bootstrap3popover.js
        /live-validator-theme-bootstrap3popover.min.js
        /live-validator-theme-bootstrap3tooltip.js
        /live-validator-theme-bootstrap3tooltip.min.js
        /live-validator-theme-uikit.js
        /live-validator-theme-uikit.min.js
        /live-validator-theme-uikittooltip.js
        /live-validator-theme-uikittooltip.min.js

    <!-- The available translations for the error messages -->
    /translations
        /af.js
        /af.min.js
        /en-us.js
        /en-us.min.js

/css
    <!-- The styles for any themes that might need them -->
    /themes
        /live-validator-theme-default.css
        /live-validator-theme-default.min.css
```
{{% alert %}}The core, a theme and the 'en-us' translation is needed at a minimum{{% /alert %}}

### Source structure
Folder | Description
-------|----------------------------------------------------------------------
hugo   | Has the files needed to create this documentation
src    | Contains all the source files
tests  | Contains all the test code

The structure of the `src` subfolder is a follow
```markup
/js
    <!-- Used to auto-detect checks from HTML5 attributes (like `min` and `max`) -->
    /autoChecks
        /AutoChecks.js

    <!-- The core that binds everything together -->
    /core
        LiveValidator.js

    <!-- This creates the vanilla JS or jQuery plugin -->
    /plugin
        LiveValidatorPlugin.js
        jqueryPlugin.js

    /tester
        <!-- Adds tests for HTML5 attributes (sort of a polyfill) - works together with AutoChecks.js -->
        html5validation.js

        <!-- The skeleton for the tester that can be extended with your own tests -->
        LiveValidatorTester.js

    <!-- All the available themes -->
    /themes
        Default.js
        Bootstrap3.js
        Bootstrap3Popover.js
        Bootstrap3Tooltip.js
        UIkit.js
        UIkitTooltip.js

    <!-- All the currently available translations -->
    /translations
        af.js
        en-us.js

    /utils
        <!-- Utils that the plugin and themes use to stay vanilla JS -->
        utils.js

/less
    <!-- The styles for any themes that might require them -->
    /themes
        /Default.less
```

#### Needed files
If you want to include the source files into your own as a bundle maybe, then you will need these at the minimum:

- LiveValidatorPlugin.js
- LiveValidator.js
- LiveValidatorTester.js
- utils.js
- en-us.js


{{% alert danger %}}Always include a theme too -  the plug-in will fallback to the default and will fail if it can not be found{{% /alert %}}
