# Live Validator

[![build status](https://gitlab.com/chesedo/LiveValidator/badges/master/build.svg)](https://gitlab.com/chesedo/LiveValidator/commits/master)
[![coverage report](https://gitlab.com/chesedo/LiveValidator/badges/master/coverage.svg)](https://gitlab.com/chesedo/LiveValidator/commits/master)

This vanilla JS form validator is a complete rewrite of its predecessor [jQuery-liveVal](https://github.com/chesedo/jQuery-liveVal). It is now theme-able, easier to extend the testers and errors are translatable. And finally, the entire code is unit tested using Jasmine.

Find the project [home page and docs](https://chesedo.gitlab.io/LiveValidator/).
